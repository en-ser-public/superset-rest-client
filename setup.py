import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="superset_api_client",
    version="0.0.1",
    author="EMCP",
    author_email="emcp@whichdegree.co",
    description="perform REST calls ",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/jrgemcp-public/superset-rest-client.git",
    packages=setuptools.find_packages(include=["bootstrap_superset*"]),
    install_requires=["requests"],
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.8',
)
